const bunyan = require('bunyan');
const fs = require('fs');
const path = require('path');

module.exports = function(filePath) {
    if (!fs.existsSync(path.dirname(filePath))) {
        fs.mkdirSync(path.dirname(filePath), 0o744);
    }
    const logger = bunyan.createLogger({
        name: 'crawler-app',
        streams: [
            {
                level: 'info',
                path: filePath // log ERROR and above to a file
            }
        ]
    });
    return logger.info.bind(logger);
};
