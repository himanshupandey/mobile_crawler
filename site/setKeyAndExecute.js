const inMemoryStorage = require('./inMemoryStorage');

module.exports = function setKeyAndExecute(key, callback){
  inMemoryStorage.get(key).then((value) => {
    if (typeof value === 'undefined') {
      return callback();
    }
    return false;
  }).then((value) => {
    if (value !== false) {
      return inMemoryStorage.set(key, true)
        .then(() => value);
    }
    return false;
  });
};
