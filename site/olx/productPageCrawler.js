const cralwerConfig = require('../../crawlConfig');
const logger = require('../../logToFile')(`${__dirname}/data/olxLeads.log`);
const setKeyAndExecute = require('../setKeyAndExecute');
const request = require('request-promise-native');

let getCurrentDate = function() {
  let now = new Date(),
      year = now.getFullYear(),
      month = now.getMonth() + 1,
      date = now.getDate();

  month = ( month + '' ).length === 2 ? month : '0' + month;
  date = ( date + '' ).length === 2 ? date : '0' + date;

  return [ year, month, date ].join('-');
};

module.exports = function productPageCrawlerCallback(crawler, olxSeed, category, city){
    return function (error, res, done) {
        const $ = res.$;
        if (error) {
            console.error(error); // eslint-disable-line no-console, no-undef
        } else {
          let itemId = ((/\"ad_id\"\:\"([0-9]*?)\"/g.exec($('body').html())) || [])[1];
          if(itemId) {
            setKeyAndExecute(`olx_item_id_${itemId}`, function(){
              let url = `https://www.olx.in/i2/ajax/ad/getcontact/?id=${itemId}&type=phone`;
              // $.get(url, function (response) {
              request({
                uri: url,
                json: true,
                proxy: 'http://lum-customer-hl_8e9d6468-zone-static:lzifbvg2ca20@zproxy.luminati.io:22225',
              }).then((response) => {
                if (response && response.urls && response.urls.phone && response.urls.phone[0] && response.urls.phone[0].uri) {
                  let userName = $('.userdatabox .userdetails .brkword').text();
                  let price = $('.offerbox .pricelabel .inlblk').text();
                  let url = res.options.uri;
                  request({
                    url: 'http://api.seller.gozefo.com/listing/zefocrawler/insert',
                    method: 'PUT',
                    json: true,
                    body: {
                      productLink: url,
                      customerName: userName,
                      customerPhone: response.urls.phone[0].uri,
                      productPrice: price,
                      dateAdded: getCurrentDate(),
                      category: category,
                      city: city,
                    }
                  }).then((response) => {
                    console.log('success ', itemId)
                  }).catch((error) => {
                    console.log(JSON.stringify(error));
                  });
                  // logger({
                  //     productLink: url,
                  //     customerName: userName,
                  //     customerPhone: response.urls.phone[0].uri,
                  //     productPrice: price,
                  //     dateAdded: getCurrentDate(),
                  //     category: category,
                  //     city: city,
                  //   });
                }
              });
            });
          }
        }
        done();
    };
};
