let olxSeeds = [
  'https://www.olx.in/bangalore/electronics-appliances/',
  'https://www.olx.in/bangalore/vehicles/',
  'https://www.olx.in/bangalore/bikes/',
  'https://www.olx.in/bangalore/kids-baby-products/',
  'https://www.olx.in/bangalore/houses/',
  'https://www.olx.in/bangalore/apartments/',
  'https://www.olx.in/bangalore/guest-houses/',
  'https://www.olx.in/bangalore/furniture/',
  'https://www.olx.in/newdelhi/electronics-appliances/',
  'https://www.olx.in/newdelhi/vehicles/',
  'https://www.olx.in/newdelhi/bikes/',
  'https://www.olx.in/newdelhi/kids-baby-products/',
  'https://www.olx.in/newdelhi/houses/',
  'https://www.olx.in/newdelhi/apartments/',
  'https://www.olx.in/newdelhi/guest-houses/',
  'https://www.olx.in/newdelhi/furniture/',
  'https://www.olx.in/noida-uttarpradesh/electronics-appliances/',
  'https://www.olx.in/noida-uttarpradesh/vehicles/',
  'https://www.olx.in/noida-uttarpradesh/bikes/',
  'https://www.olx.in/noida-uttarpradesh/kids-baby-products/',
  'https://www.olx.in/noida-uttarpradesh/houses/',
  'https://www.olx.in/noida-uttarpradesh/apartments/',
  'https://www.olx.in/noida-uttarpradesh/guest-houses/',
  'https://www.olx.in/noida-uttarpradesh/furniture/',
  'https://www.olx.in/gurgaon/electronics-appliances/',
  'https://www.olx.in/gurgaon/vehicles/',
  'https://www.olx.in/gurgaon/bikes/',
  'https://www.olx.in/gurgaon/kids-baby-products/',
  'https://www.olx.in/gurgaon/houses/',
  'https://www.olx.in/gurgaon/apartments/',
  'https://www.olx.in/gurgaon/guest-houses/',
  'https://www.olx.in/gurgaon/furniture/'
];

const cralwerConfig = require('../../crawlConfig');
const listPageCrawlerCallback = require('./listPageCrawler');

const Crawler = require('crawler');
const crawlerInstance = new Crawler(Object.assign({}, cralwerConfig, {
  // callback: function(){
  //   listPageCrawlerCallback(crawlerInstance).apply(this, arguments);
  // }
}));
olxSeeds.forEach((url) => {
  crawlerInstance.queue(Object.assign({}, cralwerConfig, {
    uri: url,
    proxy: 'http://lum-customer-hl_8e9d6468-zone-static:lzifbvg2ca20@zproxy.luminati.io:22225',
    callback: listPageCrawlerCallback(crawlerInstance, url)
  }));
});
// crawlerInstance.queue('http://www.mysmartprice.com/mobile/pricelist/mobile-price-list-in-india.html#subcategory=mobile');

crawlerInstance.on('drain', () => {
  setInterval(() => {
    if(crawlerInstance.queueSize === 0) {
      process.exit(0);
    }
    console.log('queue size', crawlerInstance.queueSize);
  }, 1000);
});

