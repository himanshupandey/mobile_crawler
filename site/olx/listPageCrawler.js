const crawlConfig = require('../../crawlConfig');
const setKeyAndExecute = require('../setKeyAndExecute');
const productPageCrawlerCallback = require('./productPageCrawler');

let titleize = function(str) {
  return str.split('-').map(function(s) {
    return s[0].toUpperCase() + s.substr(1);
  }).join('-');
};

let getCategory = function(url) {
  let pattern = /https:\/\/www.olx.in\/(?:bangalore|item|newdelhi|gurgaon|noida-uttarpradesh)\/([^\/]+)\/*/,
      matches = url.match(pattern),
      category = '';

  if(matches) {
    category = matches[1];
  }

  return titleize(category);
};

let getCity = function(url) {
  let pattern = /https:\/\/www.olx.in\/(bangalore|item|newdelhi|gurgaon|noida-uttarpradesh)\/*/,
      matches = url.match(pattern),
      city;

  if(matches) {
    city = matches[1];

    if(city === 'bangalore') { return 'Bangalore'; }
    if(city === 'noida-uttarpradesh') { return 'Noida'; }
    if(city === 'newdelhi') { return 'Delhi'; }
    if(city === 'gurgaon') { return 'Gurgoan'; }
  }

  return '';
};

module.exports = function listPageCrawler(crawler, olxSeed) {

    return function (error, res, done) {
        const $ = res.$;
        if (error) {
            console.error(error); // eslint-disable-line no-console
        } else {
          $('.listHandler table.fixed.offers .offer table.fixed .detailsLink').each(function() {
            let link = $(this).attr('href');
            if(link) {
              setKeyAndExecute(`olx_detail_url_${link}`, function () {
                crawler.queue(Object.assign({}, crawlConfig, {
                  uri: link,
                  proxy: 'http://lum-customer-hl_8e9d6468-zone-static:lzifbvg2ca20@zproxy.luminati.io:22225',
                  callback: productPageCrawlerCallback(crawler, olxSeed, getCategory(olxSeed), getCity(olxSeed)),
                }));
                return link;
              });
            }
          });
          $('.pager .item a').each(function() {
            let link = $(this).attr('href');
            if(link) {
              setKeyAndExecute(`olx_list_url_${link}`, function () {
                crawler.queue(Object.assign({}, crawlConfig, {
                  uri: link,
                  proxy: 'http://lum-customer-hl_8e9d6468-zone-static:lzifbvg2ca20@zproxy.luminati.io:22225',
                  callback: listPageCrawler(crawler, olxSeed),
                }));
                return link;
              });
            }
          })
        }
        done();
    };
};
