const NodeCache = require( "node-cache" );
const myCache = new NodeCache( { stdTTL: 72000, checkperiod: 72020 } );

module.exports = {
  set: (key, value) => {
    return new Promise((resolve, reject) => {
      myCache.set( String(key), value, function( err, success ){
        if( !err && success){
          resolve(value);
        }
        else{
          console.error(err);
          reject(err);
        }
      });
    });
  },
  get: (key) => {
    return new Promise((resolve, reject) => {
      myCache.get( String(key), function( err, value ){
        if( !err){
          resolve(value);
        }
        else{
          console.error(err);
          reject(err);
        }
      });
    })
  }
};
