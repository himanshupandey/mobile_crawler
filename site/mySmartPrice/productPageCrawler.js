const cralwerConfig = require('../../crawlConfig');
const priceHistoryCallback = require('./priceHistory');
const logger = require('../../logToFile')(`${__dirname}/data/products.log`);
const inMemoryStorage = require('../inMemoryStorage');

module.exports = function productPageCrawlerCallback(crawler){
    return function (error, res, done) {
        const $ = res.$;
        if (error) {
            console.error(error); // eslint-disable-line no-console, no-undef
            done(error);
        } else {
            $('.body-wrpr').each(function () {
                const $this = $(this);
                const productData = {};
                productData.mspid = $this.find('[itemprop=\'name\']').data('mspid');
                inMemoryStorage.get(`productId:${productData.mspid}`).then((value) => {
                    if(typeof value === 'undefined'){
                        console.log('mspid crawled ', productData.mspid);
                        productData.name = String($this.find('[itemprop=\'name\']').text()).trim();
                        productData.brand = String(($('.page-info__brdcrmb').text().split('/ ')[1])).trim();
                        productData.imageUrl = $this.find('.prdct-dtl__img').data('image');
                        productData.URL = res.options.uri;
                        productData.price = $this.find('.prdct-dtl__prc-val').text();
                        productData.internalMemory = $this.find('.avlbl-sizes__item-wrpr--slctd .avlbl-sizes__item').text();
                        productData.colors = $this.find('.avlbl-clrs__item').map(function () {
                            return $(this).attr('data-tooltip');
                        }).toArray();
                        productData.ebayPrice = $('.prc-grid[data-storename=\'ebay_1\']').data('pricerank');
                        productData.flipkartPrice = $('.prc-grid[data-storename=\'flipkart\']').data('pricerank');
                        productData.shopcluesPrice = $('.prc-grid[data-storename=\'shopclues\']').data('pricerank');
                        productData.amazonPrice = $('.prc-grid[data-storename=\'amazon\']').data('pricerank');
                        productData.cromaPrice = $('.prc-grid[data-storename=\'croma\']').data('pricerank');
                        productData.gadgetsnowPrice = $('.prc-grid[data-storename=\'gadgetsnow\']').data('pricerank');
                        productData.tatacliqPrice = $('.prc-grid[data-storename=\'tatacliq\']').data('pricerank');
                        logger(productData);

                        crawler.queue(Object.assign({}, cralwerConfig, {
                            uri: `http://www.mysmartprice.com/msp/processes/property/ui-factory/msp_generate_pricegraph.php?mspid=${productData.mspid}`,
                            callback: priceHistoryCallback(crawler, productData.mspid, productData.name),
                            jQuery: false,
                        }));
                        return productData.mspid;
                    }
                    return false;
                }).then((value) => {
                  if(value){
                      return inMemoryStorage.set(`productId:${value}`, true);
                  }
                  return false;
              });
            }); // eslint-disable-line no-console
            $('.avlbl-sizes__item-wrpr.js-open-link').each(function () {
                const url = $(this).attr('data-open-link');
               inMemoryStorage.get(`url:${url}`).then((value) => {
                  if (typeof value === 'undefined') {
                    // console.log('product url crawled avl size ', url);
                    crawler.queue(Object.assign({}, cralwerConfig, {
                        uri: url,
                        callback: productPageCrawlerCallback(crawler)
                    }));
                    return url;
                  }
                  return false;
                }).then((value) => {
                  if (value) {
                    return inMemoryStorage.set(`url:${url}`, true);
                  }
                  return false;
                });
            });
            done();
        }
    };
};
