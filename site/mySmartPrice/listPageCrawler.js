const productPageCrawlerCallback = require('./productPageCrawler');
const crawlConfig = require('../../crawlConfig');
const inMemoryStorage = require('../inMemoryStorage');

module.exports = function listPageCrawler(crawler) {

    return function (error, res, done) {
        const $ = res.$;
        if (error) {
            console.error(error); // eslint-disable-line no-console
            done(error);
        } else {
            $('.prdct-item__name').each(function() {
              let url = $(this).attr('href');
              if(url) {
                inMemoryStorage.get(`url:${url}`).then((value) => {
                  if (typeof value === 'undefined') {
                    // console.log('product url cralwed ', url);
                    crawler.queue(Object.assign({}, crawlConfig, {
                      uri: url,
                      callback: productPageCrawlerCallback(crawler)
                    }));
                    return url;
                  }
                  return false
                }).then((value) => {
                  if (value) {
                    return inMemoryStorage.set(`url:${url}`, true);
                  }
                  return false;
                });
              }
            });
            $('.pgntn__item').each(function() {
              let url = $(this).attr('href');
              if(url) {
                inMemoryStorage.get(`url:${url}`).then((value) => {
                  if (typeof value === 'undefined') {
                    // console.log('list page crawled ', url);
                    crawler.queue(Object.assign({}, crawlConfig, {
                      uri: url,
                    }));
                    return url;
                  }
                  return false;
                }).then((value) => {
                  if (value) {
                    return inMemoryStorage.set(`url:${url}`, true);
                  }
                  return false;
                });
              }
            });
            done();
        }
    };
};
