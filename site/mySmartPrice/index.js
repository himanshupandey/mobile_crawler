const cralwerConfig = require('../../crawlConfig');
const listPageCrawlerCallback = require('./listPageCrawler');

const Crawler = require('crawler');
const crawlerInstance = new Crawler(Object.assign({}, cralwerConfig, {callback: function(){
  listPageCrawlerCallback(crawlerInstance).apply(this, arguments);
}}));
crawlerInstance.queue('http://www.mysmartprice.com/mobile/pricelist/mobile-price-list-in-india.html#subcategory=mobile');
let prevQueueSize = 0;
crawlerInstance.on('drain', () => {
  setInterval(() => {
    if(crawlerInstance.queueSize === 0 || crawlerInstance.queueSize === prevQueueSize) {
      process.exit(0);
    }
    console.log('queue size', crawlerInstance.queueSize);
    console.log('Prev queue size', prevQueueSize);
    prevQueueSize = crawlerInstance.queueSize;
  }, 3000);
});
