const logger = require('../../logToFile')(`${__dirname}/data/productsHistory.log`);
const inMemoryStorage = require('../inMemoryStorage');

module.exports = function priceHistoryCrawler(crawler, mspid, name){
    return function (error, res, done) {
        if (error) {
            console.error(error);
            done(error);
        } else {
            inMemoryStorage.get(`priceHistory:${mspid}`).then((value) => {
                if (typeof value === 'undefined') {
                    let results = /response\=(\[.*?\])/g.exec(String(res.body));
                    if (results && results[1]) {
                        try {
                            // console.log('priceHistory crawled ', mspid);
                            let priceHistoryData = JSON.parse(results[1]);
                            priceHistoryData.forEach((data) => {
                                data.mspid = mspid; // eslint-disable-line no-param-reassign
                                data.name = name; // eslint-disable-line no-param-reassign
                                logger(data)
                            });
                        }
                        catch (error) {
                            console.error(error)
                            done(error);
                        }
                    }
                    return mspid;
                }
                return false;
            }).then((value) => {
                if (value) {
                    return inMemoryStorage.set(`priceHistory:${value}`, true);
                }
                return false;
            });
            done();
        }
    };
};
