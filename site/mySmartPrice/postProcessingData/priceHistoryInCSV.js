const lineReader = require('line-reader');
const fs = require('fs');
const inputFilePath = `${__dirname}/../data/productsHistory.log`;
const outPutFilePath = `${__dirname}/../data/mySmartPriceProductsHistory.csv`;
const columnArray = ['mspid', 'date', 'name', 'value'];
const Promise = require('bluebird');

let index = 0;
let eachLine = Promise.promisify(lineReader.eachLine);
eachLine(inputFilePath, function(line) {
  const dataObj = JSON.parse(line);
  if(index === 0) {
    fs.appendFile(outPutFilePath, `${columnArray.map((str) => '"' + str + '"').join('|')}\n`, console.err);
  }
  fs.appendFile(outPutFilePath, `${(columnArray.map((name) => '"' + dataObj[name] + '"').join('|'))}\n`, console.err);
  index ++;
}).then(() => {
  process.exit(0);
}).catch((error) => {
  console.error(error);
  process.exit(1);
});
