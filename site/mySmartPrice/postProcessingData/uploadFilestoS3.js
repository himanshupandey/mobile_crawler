const productLogFile = `${__dirname}/../data/products.log`;
const historyFileLogFile = `${__dirname}/../data/productsHistory.log`;
const historyFile = `${__dirname}/../data/mySmartPriceProductsHistory.csv`;
const productData = `${__dirname}/../data/mySmartPriceProductsData.csv`;
const uploadtoS3 = require('../../uploadToS3');
const fs = require('fs-promise');

Promise.all([
  uploadtoS3(productData),
  uploadtoS3(historyFile),
]).then(() => {
    console.log('Success');
    return Promise.all([
        fs.unlink(productData),
        fs.unlink(historyFile),
        fs.unlink(productLogFile),
        fs.unlink(historyFileLogFile),
      ])
      .then(() => {
        console.log('DELETE File: ', productData, ' ', historyFile, ' ', productLogFile, ' ', historyFileLogFile);
        process.exit(0);
      });
  })
  .catch((error) => {
    console.log(error);
    process.exit(1);
  });
