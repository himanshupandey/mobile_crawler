const lineReader = require('line-reader');
const fs = require('fs');
const inputFilePath = `${__dirname}/../data/products.log`;
const outPutFilePath = `${__dirname}/../data/mySmartPriceProductsData.csv`;
const Promise = require('bluebird');
const columnArray = ['mspid', 'brand', 'name', 'price', 'internalMemory', 'colors', 'ebayPrice', 'flipkartPrice', 'shopcluesPrice', 'amazonPrice', 'cromaPrice', 'gadgetsnowPrice', 'imageUrl', 'URL'];
let urlSet = new Set();

let index = 0;
let eachLine = Promise.promisify(lineReader.eachLine);
eachLine(inputFilePath, function(line) {
  const dataObj = JSON.parse(line);
  if(index === 0) {
    fs.appendFile(outPutFilePath, `${columnArray.map((str) => '"' + str + '"').join('|')}\n`, console.err);
  }
  if(!urlSet.has(dataObj.mspid)) {
    fs.appendFile(outPutFilePath, `${(columnArray.map((name) => '"' + dataObj[name] + '"').join('|'))}\n`, console.err);
    urlSet.add(dataObj.mspid);
  }
  index++;
}).then(() => {
  process.exit(0);
}).catch((error) => {
  console.error(error);
  process.exit(1);
});
