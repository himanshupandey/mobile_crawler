const s3 = require('s3');
const s3Options = require('../awsConfig');
const path = require('path');

const client = s3.createClient({
  maxAsyncS3: 20,     // this is the default
  s3RetryCount: 3,    // this is the default
  s3RetryDelay: 1000, // this is the default
  multipartUploadThreshold: 20971520, // this is the default (20 MB)
  multipartUploadSize: 15728640, // this is the default (15 MB)
  s3Options: s3Options,
});

const Bucket = 'zefo-file-backup';

module.exports = function(filePath) {
  const currentDate = new Date();
  const fileName = path.basename(filePath);
  const s3Path = `mobile_price/${currentDate.getFullYear()}/${currentDate.getMonth() + 1}/${currentDate.getDate()}/${fileName}${currentDate.getHours()}-${currentDate.getMinutes()}-${currentDate.getSeconds()}`;
  const uploader = client.uploadFile({
    localFile: filePath,
    s3Params: {
      Bucket,
      Key: s3Path,
    }
  });
  return new Promise((resolve, reject) => {
    uploader.on('error', (err) => {
      reject(err);
      console.error('Error Uploading: ', err.stack);
    });
    uploader.on('progress', () => {
      console.log('Progress: ', uploader.progressMd5Amount, uploader.progressAmount, uploader.progressTotal);
    });
    uploader.on('end', () => {
      console.log('Upload ', filePath, ' to ', s3Path);
      resolve();
    })
  })
};
