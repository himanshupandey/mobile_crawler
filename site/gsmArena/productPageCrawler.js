const logger = require('../../logToFile')(`${__dirname}/data/products.log`);
const inMemoryStorage = require('../inMemoryStorage');

module.exports = function productPageCrawlerCallback(crawler){
    return function (error, res, done) {
        const $ = res.$;
        if (error) {
            console.error(error); // eslint-disable-line no-console, no-undef
        } else {
              const productData = {};
              const urlSplits = res.options.uri.split(/[-.]/);
              urlSplits.pop();
              productData.id = urlSplits.pop();
              inMemoryStorage.get(`gsmArena:productId:${productData.id}`).then((value) => {
                  if(typeof value === 'undefined'){
                      console.log('id crawled ', productData.id);
                      productData.URL = `http://${res.connection._host}/${res.options.uri}`;
                      productData.name = $('.specs-phone-name-title').text();
                      productData.image = $('.specs-photo-main img').attr('src');
                      productData.brand = String(productData.name).split(' ')[0];
                      productData.percentile = $('.specs-spotlight-features .help-popularity strong').text()
                      productData.hits = $('.specs-spotlight-features .help-popularity span').text();
                      productData.fans = $('.specs-spotlight-features .help-fans .specs-fans strong').text()
                      let subHeading = '';
                      $('#specs-list table tr').each(function() {
                        const $this = $(this);
                        subHeading = $this.find('th').text() || subHeading;
                        const $td = $this.find('td');
                        if($($td[1]).text().trim()) {
                          productData[`${subHeading ? subHeading : ''}${subHeading && $($td[0]).text().trim() ? '--' : ''}${$($td[0]).text()}`] = $($td[1]).text().trim();
                        }
                      });
                      logger({data: productData});
                      return productData.id;
                  }
                  return false;
              }).then((value) => {
                if(value){
                    return inMemoryStorage.set(`gsmArena:productId:${value}`, true);
                }
                return false;
            });
        }
        done();
    };
};
