const cralwerConfig = require('../../crawlConfig');
const listPageCrawlerCallback = require('./listPageCrawler');
const homePageCrawlerCallback = require('./homePageCrawler');

const Crawler = require('crawler');
const crawlerInstance = new Crawler(Object.assign({}, cralwerConfig, {callback: function(){
  listPageCrawlerCallback(crawlerInstance).apply(this, arguments);
}}));
crawlerInstance.queue(Object.assign({}, cralwerConfig, {url: 'http://www.gsmarena.com', callback: homePageCrawlerCallback(crawlerInstance)}));

crawlerInstance.on('drain', () => {
  setInterval(() => {
    if(crawlerInstance.queueSize === 0) {
      process.exit(0); // eslint-disable-line no-process-exit
    }
    console.log('queue size', crawlerInstance.queueSize);
  }, 1000);
});
