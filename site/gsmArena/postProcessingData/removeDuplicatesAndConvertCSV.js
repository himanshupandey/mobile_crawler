const lineReader = require('line-reader');
const fs = require('fs');
const Promise = require('bluebird');
const inputFilePath = `${__dirname}/../data/products.log`;
const outPutFilePath = `${__dirname}/../data/gsmArenaProductsData.csv`;
let urlSet = new Set();
let columnHeadingSet = new Set();


let eachLine = Promise.promisify(lineReader.eachLine);

eachLine(inputFilePath, function(line) {
  const dataObj = JSON.parse(line);
  Object.getOwnPropertyNames(dataObj.data).forEach((key) => {
    if(!columnHeadingSet.has(key)){
      columnHeadingSet.add(key);
    }
  })
}).then(() => {
  let index = 0;
  const columnHeaderArray = Array.from(columnHeadingSet);
  return eachLine(inputFilePath, function(line) {
    const dataObj = JSON.parse(line);
    if(index === 0) {
      fs.appendFile(outPutFilePath, `${(columnHeaderArray.map((str) => '"' + str + '"').join('|'))}\n`, console.err);
    }
    if(!urlSet.has(dataObj.data.id)) {
      fs.appendFile(outPutFilePath, `${(columnHeaderArray.map((name) => dataObj.data[name]).map((str) => str?'"' + str + '"': '').join('|'))}\n`, console.err);
      urlSet.add(dataObj.data.id);
    }
    index++ ;
  });
}).then(() => {
  process.exit(0);
}).catch((error) => {
  console.error(error);
  process.exit(1);
});

