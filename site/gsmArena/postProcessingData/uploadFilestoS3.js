const productLogFile = `${__dirname}/../data/products.log`;
const productData = `${__dirname}/../data/gsmArenaProductsData.csv`;
const uploadtoS3 = require('../../uploadToS3');
const fs = require('fs-promise');

Promise.all([
  uploadtoS3(productData),
]).then(() => {
    console.log('Success');
    Promise.all([
        fs.unlink(productData),
        fs.unlink(productLogFile),
      ])
      .then(() => {
        console.log('DELETE File: ', productData, ' ', productLogFile);
        process.exit(0);
      });
  })
  .catch((error) => {
    console.log(error);
    process.exit(1);
  });
