const productPageCrawlerCallback = require('./productPageCrawler');
const crawlConfig = require('../../crawlConfig');
const inMemoryStorage = require('../inMemoryStorage');

module.exports = function listPageCrawler(crawler) {

    return function (error, res, done) {
        const $ = res.$;
        if (error) {
            console.error(error); // eslint-disable-line no-console
        } else {
            $('#review-body .makers ul li a').each(function() {
              let url = $(this).attr('href');
              if(url) {
                inMemoryStorage.get(`gsmArena:url:${url}`).then((value) => {
                  if (typeof value === 'undefined') {
                    // console.log('product url cralwed ', url);
                    crawler.queue(Object.assign({}, crawlConfig, {
                      uri: `http://${res.connection._host}/${url}`,
                      callback: productPageCrawlerCallback(crawler)
                    }));
                    return url;
                  }
                  return false
                }).then((value) => {
                  if (value) {
                    return inMemoryStorage.set(`gsmArena:url:${url}`, true);
                  }
                  return false;
                });
              }
            });
            $('.nav-pages a').each(function() {
              let url = $(this).attr('href');
              if(url) {
                inMemoryStorage.get(`gsmArena:url:${url}`).then((value) => {
                  if (typeof value === 'undefined') {
                    // console.log('list page crawled ', url);
                    crawler.queue(Object.assign({}, crawlConfig, {
                      uri: `http://${res.connection._host}/${url}`,
                      callback: listPageCrawler(crawler)
                    }));
                    return url;
                  }
                  return false;
                }).then((value) => {
                  if (value) {
                    return inMemoryStorage.set(`gsmArena:url:${url}`, true);
                  }
                  return false;
                });
              }
            });
        }
        done();
    };
};
