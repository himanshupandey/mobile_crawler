const listPageCrawlerCallback = require('./listPageCrawler');
const crawlConfig = require('../../crawlConfig');
const inMemoryStorage = require('../inMemoryStorage');

module.exports = function homePage(crawler) {

    return function (error, res, done) {
        const $ = res.$;
        if (error) {
            console.error(error); // eslint-disable-line no-console
        } else {
            $('.brandmenu-v2 ul li a').each(function() {
              let url = $(this).attr('href');
              if(url) {
                inMemoryStorage.get(`gsmArena:url:${url}`).then((value) => {
                  if (typeof value === 'undefined') {
                    // console.log('product url cralwed ', url);
                    crawler.queue(Object.assign({}, crawlConfig, {
                      uri: `http://${res.connection._host}/${url}`,
                      callback: listPageCrawlerCallback(crawler)
                    }));
                    return url;
                  }
                  return false
                }).then((value) => {
                  if (value) {
                    return inMemoryStorage.set(`gsmArena:url:${url}`, true);
                  }
                  return false;
                });
              }
            });
        }
        done();
    };
};
