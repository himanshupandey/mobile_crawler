const RATE = 10000;
const MAX_CONNECTIONS = 1;
const SKIP_DUPLICATES = false;
const jsdom = require('jsdom');

return {
    rateLimit: RATE,
    maxConnections: MAX_CONNECTIONS,
    skipDuplicates: SKIP_DUPLICATES,
    jQuery: jsdom,
    // This will be called for each crawled page
};
